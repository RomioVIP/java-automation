Feature: Tests feature

  Scenario: Complete registration procedure (TC-1)
    Given go to the authorization page
    When follow the steps to go to the page with the input of the email
    And open mail and copy email
    And enter the copied email into the line of the email address, fill in the remaining fields
    And follow the link from the letter, close the start tab
    And confirm the entered secret code
    And select secret questions and give answers to them
    And enter the user's personal data
    Then check that the name and last name are visible

  Scenario: Complete authorization procedure (TC-2)
    Given go to the authorization page
    When enter the email address and password
    And define the security question and enter the corresponding answer
    Then check that the name and last name are visible

  Scenario: check that job search in a given profession works (TC-3)
    Given open job search
    When looking for the required profession
    Then check the number of vacancies

  Scenario: check adding and deleting an alert (TC-4)
    Given go to the authorization page
    When enter the email address and password
    And define the security question and enter the corresponding answer
    And open job search
    And looking for the required profession
    And create job alert and make sure that job alert created
    Then delete job alert, check that the job alert is deleted

  Scenario: check that the alarm is created, deactivated, activated and deleted (TC-5)
    Given go to the authorization page
    When enter the email address and password
    And define the security question and enter the corresponding answer
    And open job search
    And looking for the required profession
    And create job alert and make sure that job alert created
    And deactivate job alert, check that the job alert is deactivated
    And activate job alert, check that the job alert is activated
    Then delete job alert, check that the job alert is deleted

  Scenario: create skills and knowledge profile (TC-6)
    Given go to the skills checklist page
    When confirm processing of information
    And choosing the necessary skills
    Then check that skills and knowledge profile created

  Scenario: take the quiz and download results (TC-7)
    Given go to the authorization page
    When enter the email address and password
    And define the security question and enter the corresponding answer
    And go to the career quizzes page
    And choose the first quiz
    And choose answers on the quiz pages
    Then download the quiz result

  Scenario: check wages in a given region (TC-8)
    Given go to the wages page
    When searching by city or postal code
    Then check that the wages results are visible

  Scenario: check wages by occupation and download the result (TC-9)
    Given go to the wages page
    When searching by occupation
    Then check that the results are visible
    And download occupation wages result

  Scenario: Check the diagram of employment is working (TC-10)
    Given go to the field of study page
    When choose an educational program and degree of education
    Then check the result of the diagram

  Scenario: Check that the site is translated into French (TC-11)
    Given press the website translation button
    Then check that the site is translated into French