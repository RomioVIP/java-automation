package runner;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"defs", "hooks"}

)
public class CucumberRunner {

    @AfterClass
    public static void envProps() throws IOException {
        final String filePath = System.getProperty("user.dir") +
                "/target/allure-results/environment.properties";

        final File propsFile = new File(filePath);
        if(propsFile.exists()){
            propsFile.delete();
        }

        FileOutputStream fos = new FileOutputStream(propsFile);

        Properties properties = new Properties();
        properties.setProperty("developer", "Roman Dobshikov");
        properties.setProperty("phone number", "+375(44)732-76-41");
        properties.setProperty("selenide.browser", "chrome");
        properties.setProperty("selenide.baseUrl", "https://www.jobbank.gc.ca/");
        properties.setProperty("seleniumGrid.baseUrl", "firefox");

        properties.store(fos, "allure runtime properties");
    }

}
