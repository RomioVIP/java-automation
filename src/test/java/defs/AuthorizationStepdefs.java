package defs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import services.AuthorizationService;

public class AuthorizationStepdefs {

    private final AuthorizationService authorizationService = new AuthorizationService();

    @When("enter the email address and password")
    public void inputEmailOnLoginPageStepDef() {
        authorizationService.inputEmailOnLoginPageStep();
        authorizationService.inputPasswordOnLoginPageStep();
    }

    @And("define the security question and enter the corresponding answer")
    public void inputAnswerToSecretQuestionOnLoginValidatePageStepDef() {
        authorizationService.inputAnswerToSecretQuestionOnLoginValidatePageStep();
    }

}
