package defs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import services.RegistrationService;

public class RegistrationServiceStepdefs {
    
    private final RegistrationService registrationService = new RegistrationService();
    
    @Given("go to the authorization page")
    public void signIbLikeJobSeekersStepDef() {
        registrationService.signIbLikeJobSeekersStep();
    }
    
    @When("follow the steps to go to the page with the input of the email")
    public void signUpStepDef() {
        registrationService.signUpStep();
        registrationService.standardAccountBtnClickStep();
        registrationService.iAgreeBtnClickAndCreateNewTabStep();
    }

    @And("open mail and copy email")
    public void copyMailAndSwitchTabStepDef() {
        registrationService.copyMailAndSwitchTabStep();
    }

    @And("enter the copied email into the line of the email address, fill in the remaining fields")
    public void inputEmailAndPasswordAndSwitchTabStepDef() {
        registrationService.inputEmailAndPasswordAndSwitchTabStep();
    }

    @And("follow the link from the letter, close the start tab")
    public void clickLinkAndCloseOldTabsStepDef() {
        registrationService.clickLinkAndCloseOldTabsStep();
    }

    @And("confirm the entered secret code")
    public void clickToContinueStepDef() {
        registrationService.clickToContinueStep();
    }

    @And("select secret questions and give answers to them")
    public void allActionsOnTheRegStepThreePageStepDef(){
        registrationService.allActionsOnTheRegStepThreePageStep();
    }

    @And("enter the user's personal data")
    public void allActionsOnTheRegStepFourPageStepDef() {
        registrationService.allActionsOnTheRegStepFourPageStep();
    }

    @Then("check that the name and last name are visible")
    public void authorizationCheckStepDef() {
        registrationService.authorizationCheckStep();
    }

}
