package defs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import services.WagesService;

import java.io.FileNotFoundException;

public class WagesStepdefs {

    private final WagesService wagesService = new WagesService();

    @Given("go to the wages page")
    public void openWagesSearchStepDef() {
        wagesService.openWagesSearchStep();
    }

    @When("searching by city or postal code")
    public void selectInTheSelectorEnterTheSpecifiedCityInTheInputFieldConfirmAndClickButton() {
        wagesService.chooseWagesInTheRegionStep();
    }

    @Then("check that the wages results are visible")
    public void checkThatWagesInTheRegionIsDisplayedStepDef() {
        wagesService.checkThatWagesInTheRegionIsDisplayedStep();
    }

    @When("searching by occupation")
    public void chooseOccupationWagesStepDef() {
        wagesService.chooseOccupationWagesStep();
    }

    @Then("check that the results are visible")
    public void checkThatOccupationWagesIsVisibleStepDef() {
        wagesService.checkThatOccupationWagesIsVisibleStep();
    }

    @And("download occupation wages result")
    public void downloadWadesFileStepDef() throws FileNotFoundException {
        wagesService.downloadWadesFileStep();
    }
}
