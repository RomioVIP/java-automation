package defs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import services.FieldsOfStudyService;

public class FieldsOfStudyStepdefs {

    private final FieldsOfStudyService fieldsOfStudyService = new FieldsOfStudyService();


    @Given("go to the field of study page")
    public void openSearchFieldOfStudyStepDefinition() {
        fieldsOfStudyService.openSearchFieldOfStudyStep();
    }


    @When("choose an educational program and degree of education")
    public void choiceOfEducationalProgramStepDefinition() {
        fieldsOfStudyService.choiceOfEducationalProgramStep();
    }


    @Then("check the result of the diagram")
    public void findDiagramStepDefinition() {
        fieldsOfStudyService.findDiagramStep();
    }
}
