package defs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import services.JobSearchingService;

public class JobSearchingStepdefs {

    private final JobSearchingService jobSearchingService = new JobSearchingService();

    @Given("open job search")
    public void openJobSearchingStepDef() {
        jobSearchingService.openJobSearchingStep();
    }

    @When("looking for the required profession")
    public void openSkillsChecklistStepDef() {
        jobSearchingService.openSkillsChecklistStep();
    }

    @Then("check the number of vacancies")
    public void checkTheResultsNumberStepDef() {
        jobSearchingService.checkTheResultsNumberStep();
    }
}
