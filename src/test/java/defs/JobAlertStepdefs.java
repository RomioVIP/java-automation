package defs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import services.JobAlertServices;

public class JobAlertStepdefs {

    private final JobAlertServices alertServices = new JobAlertServices();

    @And("create job alert and make sure that job alert created")
    public void createJobAlertStepDef() {
        alertServices.createJobAlertStep();
    }

    @And("deactivate job alert, check that the job alert is deactivated")
    public void deactivateJobAlertStepDef() {
        alertServices.deactivateJobAlertStep();
    }

    @And("activate job alert, check that the job alert is activated")
    public void activateJobAlertStepDef() {
        alertServices.activateJobAlertStep();
    }

    @Then("delete job alert, check that the job alert is deleted")
    public void deleteJobAlertStepDef() {
        alertServices.deleteJobAlertStep();
    }

}
