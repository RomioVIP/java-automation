package defs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import services.SkillsCheckService;

public class SkillsCheckStepdefs {

    private final SkillsCheckService skillsCheckService = new SkillsCheckService();

    @Given("go to the skills checklist page")
    public void openCareerPlaningStepDef() {
        skillsCheckService.openCareerPlaningStep();
    }

    @When("confirm processing of information")
    public void goToSkillsAndKnowledgeChecklistStepDef() {
        skillsCheckService.goToSkillsAndKnowledgeChecklistStep();
    }

    @And("choosing the necessary skills")
    public void markTheNecessaryItemsAndViewResultsStepDef() {
        skillsCheckService.markTheNecessaryItemsAndViewResultsStep();
    }

    @Then("check that skills and knowledge profile created")
    public void skillsAndKnowledgeIsVisibleStepDef() {
        skillsCheckService.skillsAndKnowledgeIsVisibleStep();
    }
}
