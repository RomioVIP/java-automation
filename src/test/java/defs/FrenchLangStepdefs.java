package defs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import services.FrenchLangService;

public class FrenchLangStepdefs {

    private final FrenchLangService frenchLangService = new FrenchLangService();

    @Given("press the website translation button")
    public void turnOnFrenchStepDef() {
        frenchLangService.turnOnFrenchStep();
    }

    @Then("check that the site is translated into French")
    public void checkIfFrenchIsEnabledStepDef() {
        frenchLangService.checkIfFrenchIsEnabledStep();
    }

}
