package defs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import services.QuizService;

public class QuizStepdefs {

    private final QuizService quizService = new QuizService();

    @And("go to the career quizzes page")
    public void goToCareerQuizzesStepDef() {
        quizService.goToCareerQuizzesStep();
    }

    @And("choose the first quiz")
    public void chooseFirstQuizStepDef() {
        quizService.chooseFirstQuizStep();
    }

    @And("choose answers on the quiz pages")
    public void chooseTheAnswersOnFirstPageStepDef() {
        quizService.chooseTheAnswersOnFirstPageStep();
        quizService.chooseTheAnswersOnSecondPageStep();
        quizService.chooseTheAnswersOnThirdPageStep();
        quizService.chooseTheAnswersOnFourthPageStep();
        quizService.chooseTheAnswersOnFifthPageStep();
    }

    @Then("download the quiz result")
    public void downloadQuizResultsStepDef() {
        quizService.downloadQuizResultsStep();
    }
}
