package pages.blocks.authorized;

import elements.Button;
import elements.Radio;
import org.openqa.selenium.By;

public class WorkPreferenceQuizPage {

    private final Radio quizQuestionRadio1 = new Radio(By.id("answerItem-0-2"));
    private final Radio quizQuestionRadio2 = new Radio(By.id("answerItem-1-0"));
    private final Radio quizQuestionRadio3 = new Radio(By.id("answerItem-2-0"));
    private final Radio quizQuestionRadio4 = new Radio(By.id("answerItem-3-3"));
    private final Radio quizQuestionRadio5 = new Radio(By.id("answerItem-4-0"));
    private final Radio quizQuestionRadio6 = new Radio(By.id("answerItem-5-2"));
    private final Radio quizQuestionRadio7 = new Radio(By.id("answerItem-6-4"));
    private final Radio quizQuestionRadio8 = new Radio(By.id("answerItem-7-2"));
    private final Radio quizQuestionRadio9 = new Radio(By.id("answerItem-8-2"));
    private final Radio quizQuestionRadio10 = new Radio(By.id("answerItem-9-4"));
    private final Radio quizQuestionRadio11 = new Radio(By.id("answerItem-10-4"));
    private final Radio quizQuestionRadio12 = new Radio(By.id("answerItem-11-4"));
    private final Radio quizQuestionRadio13 = new Radio(By.id("answerItem-12-2"));
    private final Radio quizQuestionRadio14 = new Radio(By.id("answerItem-13-2"));
    private final Radio quizQuestionRadio15 = new Radio(By.id("answerItem-14-3"));
    private final Radio quizQuestionRadio16 = new Radio(By.id("answerItem-15-2"));
    private final Radio quizQuestionRadio17 = new Radio(By.id("answerItem-16-3"));
    private final Radio quizQuestionRadio18 = new Radio(By.id("answerItem-17-2"));
    private final Radio quizQuestionRadio19 = new Radio(By.id("answerItem-18-3"));
    private final Radio quizQuestionRadio20 = new Radio(By.id("answerItem-19-3"));
    private final Radio quizQuestionRadio21 = new Radio(By.id("answerItem-20-0"));
    private final Radio quizQuestionRadio22 = new Radio(By.id("answerItem-21-2"));
    private final Radio quizQuestionRadio23 = new Radio(By.id("answerItem-22-2"));
    private final Radio quizQuestionRadio24 = new Radio(By.id("answerItem-23-2"));
    private final Radio quizQuestionRadio25 = new Radio(By.id("answerItem-24-2"));
    private final Radio quizQuestionRadio26 = new Radio(By.id("answerItem-25-0"));
    private final Radio quizQuestionRadio27 = new Radio(By.id("answerItem-26-4"));
    private final Radio quizQuestionRadio28 = new Radio(By.id("answerItem-27-3"));
    private final Radio quizQuestionRadio29 = new Radio(By.id("answerItem-28-0"));
    private final Radio quizQuestionRadio30 = new Radio(By.id("answerItem-29-3"));
    private final Radio quizQuestionRadio31 = new Radio(By.id("answerItem-30-0"));
    private final Radio quizQuestionRadio32 = new Radio(By.id("answerItem-31-3"));
    private final Radio quizQuestionRadio33 = new Radio(By.id("answerItem-32-2"));
    private final Radio quizQuestionRadio34 = new Radio(By.id("answerItem-33-0"));
    private final Radio quizQuestionRadio35 = new Radio(By.id("answerItem-34-2"));
    private final Radio quizQuestionRadio36 = new Radio(By.id("answerItem-35-0"));
    private final Radio quizQuestionRadio37 = new Radio(By.id("answerItem-36-3"));
    private final Radio quizQuestionRadio38 = new Radio(By.id("answerItem-37-2"));
    private final Radio quizQuestionRadio39 = new Radio(By.id("answerItem-38-2"));
    private final Radio quizQuestionRadio40 = new Radio(By.id("answerItem-39-3"));
    private final Radio quizQuestionRadio41 = new Radio(By.id("answerItem-40-3"));
    private final Radio quizQuestionRadio42 = new Radio(By.id("answerItem-41-2"));
    private final Radio quizQuestionRadio43 = new Radio(By.id("answerItem-42-2"));
    private final Radio quizQuestionRadio44 = new Radio(By.id("answerItem-43-0"));
    private final Radio quizQuestionRadio45 = new Radio(By.id("answerItem-44-1"));
    private final Radio quizQuestionRadio46 = new Radio(By.id("answerItem-45-0"));
    private final Radio quizQuestionRadio47 = new Radio(By.id("answerItem-46-2"));
    private final Radio quizQuestionRadio48 = new Radio(By.id("answerItem-47-2"));
    private final Radio quizQuestionRadio49 = new Radio(By.id("answerItem-48-2"));
    private final Radio quizQuestionRadio50 = new Radio(By.id("answerItem-49-0"));
    private final Button toNextPageBtn = new Button(By.id("get-next"));
    private final Button downloadBtn = new Button(By.xpath("//*[@id=\"preferenceQuizResult:j_id_2j_2_c\"]"));

    public Radio getQuizQuestionRadio1() {
        return quizQuestionRadio1;
    }

    public Radio getQuizQuestionRadio2() {
        return quizQuestionRadio2;
    }

    public Radio getQuizQuestionRadio3() {
        return quizQuestionRadio3;
    }

    public Radio getQuizQuestionRadio4() {
        return quizQuestionRadio4;
    }

    public Radio getQuizQuestionRadio5() {
        return quizQuestionRadio5;
    }

    public Radio getQuizQuestionRadio6() {
        return quizQuestionRadio6;
    }

    public Radio getQuizQuestionRadio7() {
        return quizQuestionRadio7;
    }

    public Radio getQuizQuestionRadio8() {
        return quizQuestionRadio8;
    }

    public Radio getQuizQuestionRadio9() {
        return quizQuestionRadio9;
    }

    public Radio getQuizQuestionRadio10() {
        return quizQuestionRadio10;
    }

    public Radio getQuizQuestionRadio11() {
        return quizQuestionRadio11;
    }

    public Radio getQuizQuestionRadio12() {
        return quizQuestionRadio12;
    }

    public Radio getQuizQuestionRadio13() {
        return quizQuestionRadio13;
    }

    public Radio getQuizQuestionRadio14() {
        return quizQuestionRadio14;
    }

    public Radio getQuizQuestionRadio15() {
        return quizQuestionRadio15;
    }

    public Radio getQuizQuestionRadio16() {
        return quizQuestionRadio16;
    }

    public Radio getQuizQuestionRadio17() {
        return quizQuestionRadio17;
    }

    public Radio getQuizQuestionRadio18() {
        return quizQuestionRadio18;
    }

    public Radio getQuizQuestionRadio19() {
        return quizQuestionRadio19;
    }

    public Radio getQuizQuestionRadio20() {
        return quizQuestionRadio20;
    }

    public Radio getQuizQuestionRadio21() {
        return quizQuestionRadio21;
    }

    public Radio getQuizQuestionRadio22() {
        return quizQuestionRadio22;
    }

    public Radio getQuizQuestionRadio23() {
        return quizQuestionRadio23;
    }

    public Radio getQuizQuestionRadio24() {
        return quizQuestionRadio24;
    }

    public Radio getQuizQuestionRadio25() {
        return quizQuestionRadio25;
    }

    public Radio getQuizQuestionRadio26() {
        return quizQuestionRadio26;
    }

    public Radio getQuizQuestionRadio27() {
        return quizQuestionRadio27;
    }

    public Radio getQuizQuestionRadio28() {
        return quizQuestionRadio28;
    }

    public Radio getQuizQuestionRadio29() {
        return quizQuestionRadio29;
    }

    public Radio getQuizQuestionRadio30() {
        return quizQuestionRadio30;
    }

    public Radio getQuizQuestionRadio31() {
        return quizQuestionRadio31;
    }

    public Radio getQuizQuestionRadio32() {
        return quizQuestionRadio32;
    }

    public Radio getQuizQuestionRadio33() {
        return quizQuestionRadio33;
    }

    public Radio getQuizQuestionRadio34() {
        return quizQuestionRadio34;
    }

    public Radio getQuizQuestionRadio35() {
        return quizQuestionRadio35;
    }

    public Radio getQuizQuestionRadio36() {
        return quizQuestionRadio36;
    }

    public Radio getQuizQuestionRadio37() {
        return quizQuestionRadio37;
    }

    public Radio getQuizQuestionRadio38() {
        return quizQuestionRadio38;
    }

    public Radio getQuizQuestionRadio39() {
        return quizQuestionRadio39;
    }

    public Radio getQuizQuestionRadio40() {
        return quizQuestionRadio40;
    }

    public Radio getQuizQuestionRadio41() {
        return quizQuestionRadio41;
    }

    public Radio getQuizQuestionRadio42() {
        return quizQuestionRadio42;
    }

    public Radio getQuizQuestionRadio43() {
        return quizQuestionRadio43;
    }

    public Radio getQuizQuestionRadio44() {
        return quizQuestionRadio44;
    }

    public Radio getQuizQuestionRadio45() {
        return quizQuestionRadio45;
    }

    public Radio getQuizQuestionRadio46() {
        return quizQuestionRadio46;
    }

    public Radio getQuizQuestionRadio47() {
        return quizQuestionRadio47;
    }

    public Radio getQuizQuestionRadio48() {
        return quizQuestionRadio48;
    }

    public Radio getQuizQuestionRadio49() {
        return quizQuestionRadio49;
    }

    public Radio getQuizQuestionRadio50() {
        return quizQuestionRadio50;
    }

    public Button getToNextPageBtn() {
        return toNextPageBtn;
    }

    public Button getDownloadBtn() {
        return downloadBtn;
    }
}
