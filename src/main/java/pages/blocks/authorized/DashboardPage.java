package pages.blocks.authorized;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class DashboardPage {

    private final SelenideElement careerPlanningBtn = $(By.xpath("//*[@id=\"wb-sm\"]/div/div/ul/li[2]/a"));
    private final Button careerQuizzesBtn = new Button(By.xpath("//*[@id=\"s2\"]/li[3]/a"));

    public SelenideElement getCareerPlanningBtn() {
        return careerPlanningBtn;
    }

    public Button getCareerQuizzesBtn() {
        return careerQuizzesBtn;
    }
}
