package pages.blocks.authorized;

import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

public class AuthorizedMainPage {

    private final Button signOnBtn = new Button(By.className("signon"));
    private final Label firstAndLastName = new Label(By.className("dropdown-menu"));

    public Button getSignOnBtn() {
        return signOnBtn;
    }

    public Label getFirstAndLastName() {
        return firstAndLastName;
    }
}
