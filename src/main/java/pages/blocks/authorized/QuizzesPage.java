package pages.blocks.authorized;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class QuizzesPage {

    private final ElementsCollection quizzesCollection = $$(By.className("btn-lg"));

    public ElementsCollection getQuizzesCollection() {
        return quizzesCollection;
    }
}
