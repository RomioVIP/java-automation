package pages.blocks.authorized;

import elements.Button;
import elements.Label;
import org.openqa.selenium.By;

public class JobAlertPage {

    private final Button deactivateAlertsBtn = new Button(By.className("deactivate"));
    private final Button reactivateAlertsBtn = new Button(By.className("reactivate"));
    private final Button inactiveBtn = new Button(By.partialLinkText("Inactive"));
    private final Button activeBtn = new Button(By.partialLinkText("Active"));
    private final Button deleteBtn = new Button(By.className("delete"));
    private final Label alertInfoLabel = new Label(By.id("messageblock"));

    public Button getDeactivateAlertsBtn() {
        return deactivateAlertsBtn;
    }

    public Button getReactivateAlertsBtn() {
        return reactivateAlertsBtn;
    }

    public Button getInactiveBtn() {
        return inactiveBtn;
    }

    public Button getActiveBtn() {
        return activeBtn;
    }

    public Button getDeleteBtn() {
        return deleteBtn;
    }

    public Label getAlertInfoLabel() {
        return alertInfoLabel;
    }
}
