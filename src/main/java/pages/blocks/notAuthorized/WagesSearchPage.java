package pages.blocks.notAuthorized;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Input;
import elements.Label;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class WagesSearchPage {

    private final Input inputLocation = new Input(By.id("ec-wages:regionSearchBox"));
    private final Input inputOccupation = new Input(By.id("ec-wages:wagesInput"));
    private final Button occupationOrLocationBtn = new Button(By.className("selectpicker-wrapper"));
    private final Button locationBtn = new Button(By.xpath("//*[@id=\"wages-inputs\"]/div[1]/div/div/ul/li[2]"));
    private final Button occupationBtn = new Button(By.xpath("//*[@id=\"wages-inputs\"]/div[1]/div/div/ul/li[1]"));
    private final Button selectSpecificOccupationOrLocationBtn = new Button(By.className("tt-selectable"));
    private final Button searchLocationBtn = new Button(By.id("searchWagesLocationSubmit"));
    private final Button searchOccupationBtn = new Button(By.id("searchWagesOccupationSubmit"));
    private final Button downloadWageDataBtn = new Button(By.id("downloadWagesLink"));
    private final SelenideElement downloadResultsBtn = $(By.xpath("//*[@id=\"dataset-resources\"]/ul/li[10]/div[2]/ul/li[2]/a"));
    private final Label wagesInTheSpecifiedLocationLabel = new Label(By.className("mrgn-lft-xl"));
    private final Label wagesOccupationLabel = new Label(By.id("wb-cont"));

    public Input getInputLocation() {
        return inputLocation;
    }

    public Input getInputOccupation() {
        return inputOccupation;
    }

    public Button getOccupationOrLocationBtn() {
        return occupationOrLocationBtn;
    }

    public Button getLocationBtn() {
        return locationBtn;
    }

    public Button getOccupationBtn() {
        return occupationBtn;
    }

    public Button getSelectSpecificOccupationOrLocationBtn() {
        return selectSpecificOccupationOrLocationBtn;
    }

    public Button getSearchLocationBtn() {
        return searchLocationBtn;
    }

    public Button getSearchOccupationBtn() {
        return searchOccupationBtn;
    }

    public Button getDownloadWageDataBtn() {
        return downloadWageDataBtn;
    }

    public SelenideElement getDownloadResultsBtn() {
        return downloadResultsBtn;
    }

    public Label getWagesInTheSpecifiedLocationLabel() {
        return wagesInTheSpecifiedLocationLabel;
    }

    public Label getWagesOccupationLabel() {
        return wagesOccupationLabel;
    }
}
