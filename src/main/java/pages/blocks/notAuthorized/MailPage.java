package pages.blocks.notAuthorized;

import elements.Button;
import org.openqa.selenium.By;

public class MailPage {

    private final Button copyMailBtn = new Button(By.className("addr-tool"));
    private final Button linkBtn = new Button(By.partialLinkText("indregister"));

    public Button getCopyMailBtn() {
        return copyMailBtn;
    }

    public Button getLinkBtn() {
        return linkBtn;
    }
}
