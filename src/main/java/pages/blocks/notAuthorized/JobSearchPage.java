package pages.blocks.notAuthorized;

import com.codeborne.selenide.ElementsCollection;
import elements.Button;
import elements.Input;
import elements.Label;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class JobSearchPage {

    private final Input searchInput = new Input(By.id("searchString"));
    private final Button searchBtn = new Button(By.id("searchButton"));
    private final Button createAlertBtn = new Button(By.className("jobalert-group"));
    private final Button signOnBtn = new Button(By.className("signon"));
    private final Button alertsBtn = new Button(By.xpath("//*[@id=\"j_id_25_2\"]/ul/li/ul/li[4]"));
    private final Label alertCreatedLabel = new Label(By.className("alert-info"));
    private final ElementsCollection result = $$(By.className("resultJobItem"));

    public Input getSearchInput() {
        return searchInput;
    }

    public Button getSearchBtn() {
        return searchBtn;
    }

    public Button getCreateAlertBtn() {
        return createAlertBtn;
    }

    public Button getSignOnBtn() {
        return signOnBtn;
    }

    public Button getAlertsBtn() {
        return alertsBtn;
    }

    public Label getAlertCreatedLabel() {
        return alertCreatedLabel;
    }

    public ElementsCollection getResult() {
        return result;
    }
}
