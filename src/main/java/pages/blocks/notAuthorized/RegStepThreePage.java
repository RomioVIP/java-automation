package pages.blocks.notAuthorized;

import elements.Button;
import elements.Checkbox;
import elements.Input;
import elements.Selector;
import org.openqa.selenium.By;

public class RegStepThreePage {

    private final Selector questionsSelectorOne = new Selector(By.id("securityQuestionForm:input-securityQuestion1Id"));
    private final Selector questionsSelectorTwo = new Selector(By.id("securityQuestionForm:input-securityQuestion2Id"));
    private final Selector questionsSelectorThree = new Selector(By.id("securityQuestionForm:input-securityQuestion3Id"));
    private final Selector questionsSelectorFour = new Selector(By.id("securityQuestionForm:input-securityQuestion4Id"));
    private final Selector questionsSelectorFive = new Selector(By.id("securityQuestionForm:input-securityQuestion5Id"));
    private final Checkbox questionsOneCheckbox = new Checkbox(By.id("securityQuestionForm:show-signup-password-answer1"));
    private final Checkbox questionsTwoCheckbox = new Checkbox(By.id("securityQuestionForm:show-signup-password-answer2"));
    private final Checkbox questionsThreeCheckbox = new Checkbox(By.id("securityQuestionForm:show-signup-password-answer3"));
    private final Checkbox questionsFourCheckbox = new Checkbox(By.id("securityQuestionForm:show-signup-password-answer4"));
    private final Checkbox questionsFiveCheckbox = new Checkbox(By.id("securityQuestionForm:show-signup-password-answer5"));
    private final Input firstLineInput = new Input(By.id("securityQuestionForm:input-answer1"));
    private final Input secondLineInput = new Input(By.id("securityQuestionForm:input-answer2"));
    private final Input thirdLineInput = new Input(By.id("securityQuestionForm:input-answer3"));
    private final Input fourthLineInput = new Input(By.id("securityQuestionForm:input-answer4"));
    private final Input fifthLineInput = new Input(By.id("securityQuestionForm:input-answer5"));
    private final Button continueBtn = new Button(By.id("step3"));

    public Selector getQuestionsSelectorOne() {
        return questionsSelectorOne;
    }

    public Selector getQuestionsSelectorTwo() {
        return questionsSelectorTwo;
    }

    public Selector getQuestionsSelectorThree() {
        return questionsSelectorThree;
    }

    public Selector getQuestionsSelectorFour() {
        return questionsSelectorFour;
    }

    public Selector getQuestionsSelectorFive() {
        return questionsSelectorFive;
    }

    public Checkbox getQuestionsOneCheckbox() {
        return questionsOneCheckbox;
    }

    public Checkbox getQuestionsTwoCheckbox() {
        return questionsTwoCheckbox;
    }

    public Checkbox getQuestionsThreeCheckbox() {
        return questionsThreeCheckbox;
    }

    public Checkbox getQuestionsFourCheckbox() {
        return questionsFourCheckbox;
    }

    public Checkbox getQuestionsFiveCheckbox() {
        return questionsFiveCheckbox;
    }

    public Input getFirstLineInput() {
        return firstLineInput;
    }

    public Input getSecondLineInput() {
        return secondLineInput;
    }

    public Input getThirdLineInput() {
        return thirdLineInput;
    }

    public Input getFourthLineInput() {
        return fourthLineInput;
    }

    public Input getFifthLineInput() {
        return fifthLineInput;
    }

    public Button getContinueBtn() {
        return continueBtn;
    }

}
