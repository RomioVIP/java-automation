package pages.blocks.notAuthorized;

import elements.Button;
import org.openqa.selenium.By;

public class RegInstructionsPage {

    private final Button standardAccountBtn = new Button(By.id("processStandard"));

    public Button getStandardAccountBtn() {
        return standardAccountBtn;
    }
}
