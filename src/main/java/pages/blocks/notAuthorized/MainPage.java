package pages.blocks.notAuthorized;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class MainPage {

    private final SelenideElement jobSearchBtn = $(By.xpath("//*[@id=\"wb-sm\"]/div/div/ul/li[1]/a"));
    private final SelenideElement careerPlanningBtn = $(By.xpath("//*[@id=\"wb-sm\"]/div/div/ul/li[2]/a"));
    private final SelenideElement trendAnalysisBtn = $(By.xpath("//*[@id=\"wb-sm\"]/div/div/ul/li[3]/a"));
    private final SelenideElement frenchLbl = $(By.className("cover-heading"));
    private final Button searchBtn = new Button(By.xpath("//*[@id=\"s1\"]/li[2]/a"));
    private final Button wagesBtn = new Button(By.xpath("//*[@id=\"s3\"]/li[3]/a"));
    private final Button skillsChecklistBtn = new Button(By.xpath("//*[@id=\"s2\"]/li[5]/a"));
    private final Button fieldOfStudyBtn = new Button(By.xpath("//*[@id=\"s2\"]/li[2]/a"));
    private final Button singInBtn = new Button(By.className("fullName"));
    private final Button jobSeekersBtn = new Button(By.linkText("Job seekers"));
    private final Button frenchBtn = new Button(By.className("hidden-xs"));

    public SelenideElement getJobSearchBtn() {
        return jobSearchBtn;
    }

    public SelenideElement getCareerPlanningBtn() {
        return careerPlanningBtn;
    }

    public SelenideElement getTrendAnalysisBtn() {
        return trendAnalysisBtn;
    }

    public SelenideElement getFrenchLbl() {
        return frenchLbl;
    }

    public Button getSearchBtn() {
        return searchBtn;
    }

    public Button getWagesBtn() {
        return wagesBtn;
    }

    public Button getSkillsChecklistBtn() {
        return skillsChecklistBtn;
    }

    public Button getFieldOfStudyBtn() {
        return fieldOfStudyBtn;
    }

    public Button getSingInBtn() {
        return singInBtn;
    }

    public Button getJobSeekersBtn() {
        return jobSeekersBtn;
    }

    public Button getFrenchBtn() {
        return frenchBtn;
    }
}
