package pages.blocks.notAuthorized;

import elements.Button;
import elements.Checkbox;
import elements.Input;
import elements.Selector;
import org.openqa.selenium.By;

public class RegStepFourPage {

    private final Selector maleOrFemaleSelector = new Selector(By.id("secondTierForm:input-title"));
    private final Input firstNameBtn = new Input(By.id("secondTierForm:input-firstName"));
    private final Input lastNameBtn = new Input(By.id("secondTierForm:input-lastName"));
    private final Checkbox notResidentCB = new Checkbox(By.id("secondTierForm:input-not-resident"));
    private final Button finishBtn = new Button(By.id("step4"));

    public Selector getMaleOrFemaleSelector() {
        return maleOrFemaleSelector;
    }

    public Input getFirstNameBtn() {
        return firstNameBtn;
    }

    public Input getLastNameBtn() {
        return lastNameBtn;
    }

    public Checkbox getNotResidentCB() {
        return notResidentCB;
    }

    public Button getFinishBtn() {
        return finishBtn;
    }
}
