package pages.blocks.notAuthorized;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Input;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class RegStepOnePage {

    private final SelenideElement inputEmail = $(By.id("registerForm:input-email"));
    private final Input inputPassword = new Input(By.id("registerForm:input-password"));
    private final Input inputConfirmPassword = new Input(By.id("registerForm:input-password-confirm"));
    private final Button continueBtn = new Button(By.id("step1"));

    public SelenideElement getInputEmail() {
        return inputEmail;
    }

    public Input getInputPassword() {
        return inputPassword;
    }

    public Input getInputConfirmPassword() {
        return inputConfirmPassword;
    }

    public Button getContinueBtn() {
        return continueBtn;
    }
}
