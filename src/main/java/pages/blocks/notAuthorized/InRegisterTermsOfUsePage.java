package pages.blocks.notAuthorized;

import elements.Button;
import org.openqa.selenium.By;

public class InRegisterTermsOfUsePage {

    private final Button agreeBtn = new Button(By.id("step1"));

    public Button getAgreeBtn() {
        return agreeBtn;
    }
}
