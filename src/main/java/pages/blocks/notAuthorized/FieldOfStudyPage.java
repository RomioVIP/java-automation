package pages.blocks.notAuthorized;

import elements.Button;
import elements.Input;
import elements.Label;

import org.openqa.selenium.By;

public class FieldOfStudyPage {

    private final Input educationProgramInput = new Input(By.id("editprofileForm:fos-name"));
    private final Button programSelectionBtn = new Button(By.xpath("//*[@id=\"editprofileForm:new-education\"]/div[1]/div[1]/div[1]/span[1]/div/div/p[1]"));
    private final Button bachelorsDegreeBtn = new Button(By.className("bachelors"));
    private final Label diagram = new Label(By.id("tblChart"));

    public Input getEducationProgramInput() {
        return educationProgramInput;
    }

    public Button getProgramSelectionBtn() {
        return programSelectionBtn;
    }

    public Button getBachelorsDegreeBtn() {
        return bachelorsDegreeBtn;
    }

    public Label getDiagram() {
        return diagram;
    }
}
