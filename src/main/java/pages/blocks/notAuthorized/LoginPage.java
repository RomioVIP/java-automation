package pages.blocks.notAuthorized;

import elements.Button;
import elements.Input;
import org.openqa.selenium.By;

public class LoginPage {

    private final Input inputEmail = new Input(By.id("loginForm:input-email"));
    private final Input inputPassword = new Input(By.id("loginForm:input-password"));
    private final Button nextBtn = new Button(By.id("loginForm:loginProcess"));
    private final Button signInBtn = new Button(By.id("loginStandardPlusUser"));
    private final Button signUpBtn = new Button(By.className("newAccountLink"));

    public Input getInputEmail() {
        return inputEmail;
    }

    public Input getInputPassword() {
        return inputPassword;
    }

    public Button getNextBtn() {
        return nextBtn;
    }

    public Button getSignInBtn() {
        return signInBtn;
    }

    public Button getSignUpBtn() {
        return signUpBtn;
    }
}
