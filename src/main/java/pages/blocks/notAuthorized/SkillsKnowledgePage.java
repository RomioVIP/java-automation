package pages.blocks.notAuthorized;

import elements.Button;
import elements.Checkbox;
import elements.Label;
import org.openqa.selenium.By;

public class SkillsKnowledgePage {

    private final Button checklistAccessBtn = new Button(By.className("fa-check-square-o"));
    private final Button viewResultsBtn = new Button(By.id("skillsKnowledge:viewresults"));
    private final Button viewYourSkillsAndKnowledgeProfileBtn = new Button(By.id("wb-auto-2"));
    private final Label skillsLbl = new Label(By.xpath("//*[@id=\"skillsKnowledgeAccordion\"]/details/div/div/div[1]/div/header/h5/span"));
    private final Label knowledgeLbl = new Label(By.xpath("//*[@id=\"skillsKnowledgeAccordion\"]/details/div/div/div[2]/div/header/h5/span"));
    private final Checkbox mathematicsCB = new Checkbox(By.id("knowledge-element-201627"));
    private final Checkbox statisticsCB = new Checkbox(By.id("knowledge-element-201628"));
    private final Checkbox sortingCB = new Checkbox(By.id("skill-element-201655"));
    private final Checkbox loadingAndUnloadingCB = new Checkbox(By.id("skill-element-201656"));
    private final Checkbox mechanicalInstallingCB = new Checkbox(By.id("skill-element-201673"));
    private final Checkbox operatingMobileEquipmentCB = new Checkbox(By.id("skill-element-201674"));
    private final Checkbox operatingStationaryIndustrialEquipmentCB = new Checkbox(By.id("skill-element-201675"));
    private final Checkbox counsellingAndNurturingCB = new Checkbox(By.id("skill-element-201676"));
    private final Checkbox treatingPeopleOrAnimalsCB = new Checkbox(By.id("skill-element-201677"));
    private final Checkbox protectingAndEnforcingCB = new Checkbox(By.id("skill-element-201678"));
    private final Checkbox negotiatingAndAdjudicatingCB = new Checkbox(By.id("skill-element-201659"));
    private final Checkbox liaisingAndNetworkingCB = new Checkbox(By.id("skill-element-201660"));
    private final Checkbox engineeringAndAppliedTechnologiesCB = new Checkbox(By.id("knowledge-element-201615"));
    private final Checkbox computerAndInformationSystemsCB = new Checkbox(By.id("knowledge-element-201616"));
    private final Checkbox languagesCB = new Checkbox(By.id("knowledge-element-201633"));
    private final Checkbox languagesForeignCB = new Checkbox(By.id("knowledge-element-201634"));
    private final Checkbox psychologyCB = new Checkbox(By.id("knowledge-element-201635"));

    public Button getChecklistAccessBtn() {
        return checklistAccessBtn;
    }

    public Button getViewResultsBtn() {
        return viewResultsBtn;
    }

    public Button getViewYourSkillsAndKnowledgeProfileBtn() {
        return viewYourSkillsAndKnowledgeProfileBtn;
    }

    public Label getSkillsLbl() {
        return skillsLbl;
    }

    public Label getKnowledgeLbl() {
        return knowledgeLbl;
    }

    public Checkbox getMathematicsCB() {
        return mathematicsCB;
    }

    public Checkbox getStatisticsCB() {
        return statisticsCB;
    }

    public Checkbox getSortingCB() {
        return sortingCB;
    }

    public Checkbox getLoadingAndUnloadingCB() {
        return loadingAndUnloadingCB;
    }

    public Checkbox getMechanicalInstallingCB() {
        return mechanicalInstallingCB;
    }

    public Checkbox getOperatingMobileEquipmentCB() {
        return operatingMobileEquipmentCB;
    }

    public Checkbox getOperatingStationaryIndustrialEquipmentCB() {
        return operatingStationaryIndustrialEquipmentCB;
    }

    public Checkbox getCounsellingAndNurturingCB() {
        return counsellingAndNurturingCB;
    }

    public Checkbox getTreatingPeopleOrAnimalsCB() {
        return treatingPeopleOrAnimalsCB;
    }

    public Checkbox getProtectingAndEnforcingCB() {
        return protectingAndEnforcingCB;
    }

    public Checkbox getNegotiatingAndAdjudicatingCB() {
        return negotiatingAndAdjudicatingCB;
    }

    public Checkbox getLiaisingAndNetworkingCB() {
        return liaisingAndNetworkingCB;
    }

    public Checkbox getEngineeringAndAppliedTechnologiesCB() {
        return engineeringAndAppliedTechnologiesCB;
    }

    public Checkbox getComputerAndInformationSystemsCB() {
        return computerAndInformationSystemsCB;
    }

    public Checkbox getLanguagesCB() {
        return languagesCB;
    }

    public Checkbox getLanguagesForeignCB() {
        return languagesForeignCB;
    }

    public Checkbox getPsychologyCB() {
        return psychologyCB;
    }
}
