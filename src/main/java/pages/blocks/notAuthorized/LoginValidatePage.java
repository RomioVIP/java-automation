package pages.blocks.notAuthorized;

import com.codeborne.selenide.SelenideElement;
import elements.Button;
import elements.Checkbox;
import elements.Input;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class LoginValidatePage {

    private final SelenideElement secretQuestion = $(By.className("lead"));
    private final Input inputAnswerToSecretQuestion = new Input(By.id("securityForm:input-security-answer"));
    private final Button continueBtn = new Button(By.id("continueButton"));
    private final Checkbox visibleAnswerBtn = new Checkbox(By.id("securityForm:show-login-challenge-answer"));

    public SelenideElement getSecretQuestion() {
        return secretQuestion;
    }

    public Input getInputAnswerToSecretQuestion() {
        return inputAnswerToSecretQuestion;
    }

    public Button getContinueBtn() {
        return continueBtn;
    }

    public Checkbox getVisibleAnswerBtn() {
        return visibleAnswerBtn;
    }
}
