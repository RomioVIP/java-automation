package pages.blocks.notAuthorized;

import elements.Button;
import org.openqa.selenium.By;

public class RegStepTwoPage {

    private final Button continueBtn = new Button(By.id("step2"));

    public Button getContinueBtn() {
        return continueBtn;
    }
}
