package services;

import pages.blocks.authorized.DashboardPage;
import pages.blocks.authorized.QuizzesPage;
import pages.blocks.authorized.WorkPreferenceQuizPage;
import utils.Logger;

import static com.codeborne.selenide.Selenide.actions;

public class QuizService {

    private final DashboardPage dashboardPage = new DashboardPage();
    private final QuizzesPage quizzesPage = new QuizzesPage();
    private final WorkPreferenceQuizPage workPreferenceQuizPage = new WorkPreferenceQuizPage();

    public void goToCareerQuizzesStep() {
        Logger.info("open the \"Career planning\" tab");
        actions().moveToElement(dashboardPage.getCareerPlanningBtn()).build().perform();
        Logger.info("click the \"Career quizzes\" button");
        dashboardPage.getCareerQuizzesBtn().click();
    }

    public void chooseFirstQuizStep() {
        Logger.info("choose the first quiz");
        quizzesPage.getQuizzesCollection().get(0).click();
    }

    public void chooseTheAnswersOnFirstPageStep() {
        Logger.info("choose the answers on first quiz page");
        workPreferenceQuizPage.getQuizQuestionRadio1().check();
        workPreferenceQuizPage.getQuizQuestionRadio2().check();
        workPreferenceQuizPage.getQuizQuestionRadio3().check();
        workPreferenceQuizPage.getQuizQuestionRadio4().check();
        workPreferenceQuizPage.getQuizQuestionRadio5().check();
        workPreferenceQuizPage.getQuizQuestionRadio6().check();
        workPreferenceQuizPage.getQuizQuestionRadio7().check();
        workPreferenceQuizPage.getQuizQuestionRadio8().check();
        workPreferenceQuizPage.getQuizQuestionRadio9().check();
        workPreferenceQuizPage.getQuizQuestionRadio10().check();
        Logger.info("click \"Next\" button");
        workPreferenceQuizPage.getToNextPageBtn().click();
    }

    public void chooseTheAnswersOnSecondPageStep() {
        Logger.info("choose the answers on second quiz page");
        workPreferenceQuizPage.getQuizQuestionRadio11().check();
        workPreferenceQuizPage.getQuizQuestionRadio12().check();
        workPreferenceQuizPage.getQuizQuestionRadio13().check();
        workPreferenceQuizPage.getQuizQuestionRadio14().check();
        workPreferenceQuizPage.getQuizQuestionRadio15().check();
        workPreferenceQuizPage.getQuizQuestionRadio16().check();
        workPreferenceQuizPage.getQuizQuestionRadio17().check();
        workPreferenceQuizPage.getQuizQuestionRadio18().check();
        workPreferenceQuizPage.getQuizQuestionRadio19().check();
        workPreferenceQuizPage.getQuizQuestionRadio20().check();
        Logger.info("click \"Next\" button");
        workPreferenceQuizPage.getToNextPageBtn().click();
    }

    public void chooseTheAnswersOnThirdPageStep() {
        Logger.info("choose the answers on third quiz page");
        workPreferenceQuizPage.getQuizQuestionRadio21().check();
        workPreferenceQuizPage.getQuizQuestionRadio22().check();
        workPreferenceQuizPage.getQuizQuestionRadio23().check();
        workPreferenceQuizPage.getQuizQuestionRadio24().check();
        workPreferenceQuizPage.getQuizQuestionRadio25().check();
        workPreferenceQuizPage.getQuizQuestionRadio26().check();
        workPreferenceQuizPage.getQuizQuestionRadio27().check();
        workPreferenceQuizPage.getQuizQuestionRadio28().check();
        workPreferenceQuizPage.getQuizQuestionRadio29().check();
        workPreferenceQuizPage.getQuizQuestionRadio30().check();
        Logger.info("click \"Next\" button");
        workPreferenceQuizPage.getToNextPageBtn().click();
    }

    public void chooseTheAnswersOnFourthPageStep() {
        Logger.info("choose the answers on fourth quiz page");
        workPreferenceQuizPage.getQuizQuestionRadio31().check();
        workPreferenceQuizPage.getQuizQuestionRadio32().check();
        workPreferenceQuizPage.getQuizQuestionRadio33().check();
        workPreferenceQuizPage.getQuizQuestionRadio34().check();
        workPreferenceQuizPage.getQuizQuestionRadio35().check();
        workPreferenceQuizPage.getQuizQuestionRadio36().check();
        workPreferenceQuizPage.getQuizQuestionRadio37().check();
        workPreferenceQuizPage.getQuizQuestionRadio38().check();
        workPreferenceQuizPage.getQuizQuestionRadio39().check();
        workPreferenceQuizPage.getQuizQuestionRadio40().check();
        Logger.info("click \"Next\" button");
        workPreferenceQuizPage.getToNextPageBtn().click();
    }

    public void chooseTheAnswersOnFifthPageStep() {
        Logger.info("choose the answers on fifth quiz page");
        workPreferenceQuizPage.getQuizQuestionRadio41().check();
        workPreferenceQuizPage.getQuizQuestionRadio42().check();
        workPreferenceQuizPage.getQuizQuestionRadio43().check();
        workPreferenceQuizPage.getQuizQuestionRadio44().check();
        workPreferenceQuizPage.getQuizQuestionRadio45().check();
        workPreferenceQuizPage.getQuizQuestionRadio46().check();
        workPreferenceQuizPage.getQuizQuestionRadio47().check();
        workPreferenceQuizPage.getQuizQuestionRadio48().check();
        workPreferenceQuizPage.getQuizQuestionRadio49().check();
        workPreferenceQuizPage.getQuizQuestionRadio50().check();
        Logger.info("click \"Next\" button");
        workPreferenceQuizPage.getToNextPageBtn().click();
    }

    public void downloadQuizResultsStep() {
        Logger.info("click \"Download\" button");
        workPreferenceQuizPage.getDownloadBtn().click();
    }
}
