package services;

import pages.blocks.notAuthorized.MainPage;
import pages.blocks.notAuthorized.SkillsKnowledgePage;
import utils.Logger;

import static com.codeborne.selenide.Selenide.actions;

public class SkillsCheckService {

    private final MainPage mainPage = new MainPage();
    private final SkillsKnowledgePage skillsKnowledgePage = new SkillsKnowledgePage();

    public void openCareerPlaningStep() {
        Logger.info("open the \"Career planning\" tab");
        actions().moveToElement(mainPage.getCareerPlanningBtn()).build().perform();
        Logger.info("click the \"Skills checklist\" button");
        mainPage.getSkillsChecklistBtn().click();
    }

    public void goToSkillsAndKnowledgeChecklistStep() {
        Logger.info("click \"Access the skills and knowledge checklist\" button");
        skillsKnowledgePage.getChecklistAccessBtn().click();
    }

    public void markTheNecessaryItemsAndViewResultsStep() {
        Logger.info("choosing the necessary skills");
        skillsKnowledgePage.getMathematicsCB().check();
        skillsKnowledgePage.getStatisticsCB().check();
        skillsKnowledgePage.getSortingCB().check();
        skillsKnowledgePage.getLoadingAndUnloadingCB().check();
        skillsKnowledgePage.getMechanicalInstallingCB().check();
        skillsKnowledgePage.getOperatingMobileEquipmentCB().check();
        skillsKnowledgePage.getOperatingStationaryIndustrialEquipmentCB().check();
        skillsKnowledgePage.getCounsellingAndNurturingCB().check();
        skillsKnowledgePage.getTreatingPeopleOrAnimalsCB().check();
        skillsKnowledgePage.getProtectingAndEnforcingCB().check();
        skillsKnowledgePage.getNegotiatingAndAdjudicatingCB().check();
        skillsKnowledgePage.getLiaisingAndNetworkingCB().check();
        skillsKnowledgePage.getEngineeringAndAppliedTechnologiesCB().check();
        skillsKnowledgePage.getComputerAndInformationSystemsCB().check();
        skillsKnowledgePage.getLanguagesCB().check();
        skillsKnowledgePage.getLanguagesForeignCB().check();
        skillsKnowledgePage.getPsychologyCB().check();
        skillsKnowledgePage.getViewResultsBtn().click();
    }

    public void skillsAndKnowledgeIsVisibleStep() {
        Logger.info("expand \"skills and knowledge\" tab");
        skillsKnowledgePage.getViewYourSkillsAndKnowledgeProfileBtn().click();
        Logger.info("check that skills and knowledge profile created");
        skillsKnowledgePage.getSkillsLbl().isDisplayedWithWait();
        skillsKnowledgePage.getKnowledgeLbl().isDisplayedWithWait();
    }
}
