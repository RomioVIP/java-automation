package services;

import com.codeborne.selenide.Condition;
import pages.blocks.notAuthorized.MainPage;
import properties.PropertyManager;
import utils.Logger;

public class FrenchLangService {

    private final MainPage mainPage = new MainPage();
    private static final String FRENCH_LOGO = PropertyManager.getProperty(PropertyManager.Props.FRENCH_LOGO);


    public void turnOnFrenchStep() {
        Logger.info("click the button \"French\"");
        mainPage.getFrenchBtn().click();
    }

    public void checkIfFrenchIsEnabledStep() {
        Logger.info("check that the site is translated into French");
        mainPage.getFrenchLbl().shouldBe(Condition.visible).shouldHave(
                Condition.text(FRENCH_LOGO));
    }
}
