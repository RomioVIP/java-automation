package services;

import pages.blocks.notAuthorized.FieldOfStudyPage;
import pages.blocks.notAuthorized.MainPage;
import properties.PropertyManager;
import utils.Logger;

import static com.codeborne.selenide.Selenide.actions;

public class FieldsOfStudyService {

    private final MainPage mainPage = new MainPage();
    private final FieldOfStudyPage fieldOfStudyPage = new FieldOfStudyPage();
    private static final String EDUCATION_PROGRAM_NAME = PropertyManager.getProperty(PropertyManager.Props.EDUCATION_PROGRAM_NAME);

    public void openSearchFieldOfStudyStep() {
        Logger.info("click the \"career planning\" tab");
        actions().moveToElement(mainPage.getCareerPlanningBtn()).build().perform();
        Logger.info("click the \"field of study\" button");
        mainPage.getFieldOfStudyBtn().click();
    }

    public void choiceOfEducationalProgramStep() {
        Logger.info("transfer the name of the \"education program\" in the input field");
        fieldOfStudyPage.getEducationProgramInput().clearAndType(EDUCATION_PROGRAM_NAME);
        Logger.info("click the \"confirmation\" button");
        fieldOfStudyPage.getProgramSelectionBtn().click();
        Logger.info("select the \"degree\" button");
        fieldOfStudyPage.getBachelorsDegreeBtn().click();
    }

    public void findDiagramStep() {
        Logger.info("check that the \"diagram\" is visible");
        fieldOfStudyPage.getDiagram().isDisplayedWithWait();
    }
}
