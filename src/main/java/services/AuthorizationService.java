package services;

import enums.Questions;
import pages.blocks.notAuthorized.LoginPage;
import pages.blocks.notAuthorized.LoginValidatePage;
import properties.PropertyManager;
import utils.Logger;

public class AuthorizationService {

    private final LoginPage loginPage = new LoginPage();
    private final LoginValidatePage loginValidatePage = new LoginValidatePage();
    private static final String EMAIL = PropertyManager.getProperty(PropertyManager.Props.EMAIL);
    private static final String PASSWORD = PropertyManager.getProperty(PropertyManager.Props.PASSWORD);
    private static final String ANSWER_Q1 = PropertyManager.getProperty(PropertyManager.Props.ANSWER_Q1);
    private static final String ANSWER_Q4 = PropertyManager.getProperty(PropertyManager.Props.ANSWER_Q4);
    private static final String ANSWER_Q11 = PropertyManager.getProperty(PropertyManager.Props.ANSWER_Q11);
    private static final String ANSWER_Q24 = PropertyManager.getProperty(PropertyManager.Props.ANSWER_Q24);
    private static final String ANSWER_Q39 = PropertyManager.getProperty(PropertyManager.Props.ANSWER_Q39);

    public void inputEmailOnLoginPageStep() {
        Logger.info("enter the email address in the email input line");
        loginPage.getInputEmail().clearAndType(EMAIL);
        Logger.info("click \"Next\" button");
        loginPage.getNextBtn().click();
    }

    public void inputPasswordOnLoginPageStep() {
        Logger.info("enter the password in the password input line");
        loginPage.getInputPassword().clearAndType(PASSWORD,50000);
        Logger.info("press the \"Sign in\" button");
        loginPage.getSignInBtn().click();
    }

    public void inputAnswerToSecretQuestionOnLoginValidatePageStep() {
        Logger.info("make visible entry field for the answer to the security question");
        loginValidatePage.getVisibleAnswerBtn().check();
        Logger.info("define the security question");
        Questions questionsFromText = Questions.findByTakenText(loginValidatePage.getSecretQuestion().getText());
        switch (questionsFromText) {
            case Q39:
                Logger.info("enter the corresponding answer");
                loginValidatePage.getInputAnswerToSecretQuestion().clearAndType(ANSWER_Q39);
                break;
            case Q1:
                Logger.info("enter the corresponding answer");
                loginValidatePage.getInputAnswerToSecretQuestion().clearAndType(ANSWER_Q1);
                break;
            case Q24:
                Logger.info("enter the corresponding answer");
                loginValidatePage.getInputAnswerToSecretQuestion().clearAndType(ANSWER_Q24);
                break;
            case Q11:
                Logger.info("enter the corresponding answer");
                loginValidatePage.getInputAnswerToSecretQuestion().clearAndType(ANSWER_Q11);
                break;
            case Q4:
                Logger.info("enter the corresponding answer");
                loginValidatePage.getInputAnswerToSecretQuestion().clearAndType(ANSWER_Q4);
                break;
        }
        Logger.info("press the \"Continue\" button");
        loginValidatePage.getContinueBtn().click();
    }
}
