package services;

import enums.Gender;
import enums.Questions;
import org.openqa.selenium.Keys;
import pages.blocks.authorized.AuthorizedMainPage;
import pages.blocks.notAuthorized.*;
import properties.PropertyManager;
import utils.Logger;

import java.awt.*;
import java.awt.event.KeyEvent;

import static com.codeborne.selenide.Selenide.*;

public class RegistrationService {

    private final MainPage mainPage = new MainPage();
    private final RegInstructionsPage regInstructionsPage = new RegInstructionsPage();
    private final LoginPage loginPage = new LoginPage();
    private final InRegisterTermsOfUsePage inRegisterTermsOfUsePage = new InRegisterTermsOfUsePage();
    private final MailPage mailPage = new MailPage();
    private final RegStepOnePage regStepOnePage = new RegStepOnePage();
    private final RegStepTwoPage regStepTwoPage = new RegStepTwoPage();
    private final RegStepThreePage regStepThreePage = new RegStepThreePage();
    private final RegStepFourPage regStepFourPage = new RegStepFourPage();
    private final AuthorizedMainPage authorizedMainPage = new AuthorizedMainPage();
    private static final String MAIL_URL = PropertyManager.getProperty(PropertyManager.Props.MAIL_URL);
    private static final String PASSWORD = PropertyManager.getProperty(PropertyManager.Props.PASSWORD);
    private static final String USER_FIRST_NAME = PropertyManager.getProperty(PropertyManager.Props.USER_FIRST_NAME);
    private static final String USER_LAST_NAME = PropertyManager.getProperty(PropertyManager.Props.USER_LAST_NAME);
    private static final String ANSWER_Q1 = PropertyManager.getProperty(PropertyManager.Props.ANSWER_Q1);
    private static final String ANSWER_Q4 = PropertyManager.getProperty(PropertyManager.Props.ANSWER_Q4);
    private static final String ANSWER_Q11 = PropertyManager.getProperty(PropertyManager.Props.ANSWER_Q11);
    private static final String ANSWER_Q24 = PropertyManager.getProperty(PropertyManager.Props.ANSWER_Q24);
    private static final String ANSWER_Q39 = PropertyManager.getProperty(PropertyManager.Props.ANSWER_Q39);
    private Robot robot;

    public void signIbLikeJobSeekersStep() {
        Logger.info("Click on \"Sign in\" button, ");
        mainPage.getSingInBtn().click();
        Logger.info("Click on \"Job seekers\" button");
        mainPage.getJobSeekersBtn().click();
    }

    public void signUpStep() {
        Logger.info("click on \"Sign in now!\" button");
        loginPage.getSignUpBtn().click();
    }

    public void standardAccountBtnClickStep() {
        Logger.info("click on \"Standard account\" button");
        regInstructionsPage.getStandardAccountBtn().click();
    }

    public void iAgreeBtnClickAndCreateNewTabStep() {
        Logger.info("click on \"I agree\" button");
        inRegisterTermsOfUsePage.getAgreeBtn().click();
        Logger.info("create a new tab");
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_T);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_T);
        Logger.info("switch tab");
        switchTo().window(1);
    }

    public void copyMailAndSwitchTabStep() {
        Logger.info("open mail");
        open(MAIL_URL);
        Logger.info("copy mail address");
        mailPage.getCopyMailBtn().click();
        Logger.info("switch tab");
        switchTo().window(0);
    }

    public void inputEmailAndPasswordAndSwitchTabStep() {
        Logger.info("enter the copied email into the line of the email address");
        regStepOnePage.getInputEmail().sendKeys(Keys.CONTROL + "v");
        Logger.info(" set the password");
        regStepOnePage.getInputPassword().clearAndType(PASSWORD);
        Logger.info("confirm the specified password");
        regStepOnePage.getInputConfirmPassword().clearAndType(PASSWORD);
        Logger.info("click the \"Continue\" button");
        regStepOnePage.getContinueBtn().click();
        Logger.info("switch tab");
        switchTo().window(1);
    }

    public void clickLinkAndCloseOldTabsStep() {
        Logger.info("click on the confirmation link");
        mailPage.getLinkBtn().click();
        switchTo().window(0);
        Logger.info("close the start window");
        closeWindow();
        Logger.info("switch tab");
        switchTo().window(1);
    }

    public void clickToContinueStep() {
        Logger.info("click the \"Continue\" button");
        regStepTwoPage.getContinueBtn().click();
    }

    public void allActionsOnTheRegStepThreePageStep(){
        Logger.info("select security questions");
        chooseQuestions();
        Logger.info("make input fields visible");
        selectAllCheckboxes();
        Logger.info("enter answers to security questions");
        inputAllAnswers();
        Logger.info("click the \"Continue\" button");
        regStepThreePage.getContinueBtn().click();
    }

    public void allActionsOnTheRegStepFourPageStep() {
        Logger.info("choose gender");
        chooseGender(Gender.MALE);
        Logger.info("set name");
        regStepFourPage.getFirstNameBtn().clearAndType(USER_FIRST_NAME);
        Logger.info("set last name");
        regStepFourPage.getLastNameBtn().clearAndType(USER_LAST_NAME);
        Logger.info("choose \"not resident of Canada\"");
        regStepFourPage.getNotResidentCB().check();
        Logger.info("click the \"Finish\" button");
        regStepFourPage.getFinishBtn().click();
    }

    public void authorizationCheckStep() {
        Logger.info("press the \"Sign on\" button");
        authorizedMainPage.getSignOnBtn().click();
        Logger.info("check that the name and last name are visible");
        authorizedMainPage.getFirstAndLastName().isDisplayedWithWait();
    }

    public void chooseQuestionOne(final Questions questions) {
        regStepThreePage.getQuestionsSelectorOne().select(questions.getQuestion());
    }

    public void chooseQuestionTwo(final Questions questions) {
        regStepThreePage.getQuestionsSelectorTwo().select(questions.getQuestion());
    }

    public void chooseQuestionThree(final Questions questions) {
        regStepThreePage.getQuestionsSelectorThree().select(questions.getQuestion());
    }

    public void chooseQuestionFour(final Questions questions) {
        regStepThreePage.getQuestionsSelectorFour().select(questions.getQuestion());
    }

    public void chooseQuestionFive(final Questions questions) {
        regStepThreePage.getQuestionsSelectorFive().select(questions.getQuestion());
    }

    public void chooseQuestions(){
        chooseQuestionOne(Questions.Q39);
        chooseQuestionTwo(Questions.Q1);
        chooseQuestionThree(Questions.Q24);
        chooseQuestionFour(Questions.Q11);
        chooseQuestionFive(Questions.Q4);
    }

    public void selectAllCheckboxes() {
        regStepThreePage.getQuestionsOneCheckbox().check();
        regStepThreePage.getQuestionsTwoCheckbox().check();
        regStepThreePage.getQuestionsThreeCheckbox().check();
        regStepThreePage.getQuestionsFourCheckbox().check();
        regStepThreePage.getQuestionsFiveCheckbox().check();
    }

    public void inputAllAnswers() {
        regStepThreePage.getFirstLineInput().clearAndType(ANSWER_Q39);
        regStepThreePage.getSecondLineInput().clearAndType(ANSWER_Q1);
        regStepThreePage.getThirdLineInput().clearAndType(ANSWER_Q24);
        regStepThreePage.getFourthLineInput().clearAndType(ANSWER_Q11);
        regStepThreePage.getFifthLineInput().clearAndType(ANSWER_Q4);
    }

    public void chooseGender(final Gender gender) {
        regStepFourPage.getMaleOrFemaleSelector().select(gender.getGender());
    }
}
