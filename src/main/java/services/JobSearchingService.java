package services;

import com.codeborne.selenide.CollectionCondition;
import pages.blocks.notAuthorized.JobSearchPage;
import pages.blocks.notAuthorized.MainPage;
import properties.PropertyManager;
import utils.Logger;

import static com.codeborne.selenide.Selenide.actions;

public class JobSearchingService {

    private final MainPage mainPage = new MainPage();
    private final JobSearchPage jobSearchPage = new JobSearchPage();
    private static final String PROFESSION_FOR_SEARCH = PropertyManager.getProperty(PropertyManager.Props.PROFESSION_FOR_SEARCH);


    public void openJobSearchingStep() {
        Logger.info("open the \"Job search\" tab");
        actions().moveToElement(mainPage.getJobSearchBtn()).build().perform();
        Logger.info("click the \"Search\" button");
        mainPage.getSearchBtn().click();
    }

    public void openSkillsChecklistStep() {
        Logger.info("in the line \"What:\" enter the required profession");
        jobSearchPage.getSearchInput().clearAndType(PROFESSION_FOR_SEARCH);
        Logger.info("click the \"Search\" button");
        jobSearchPage.getSearchBtn().click();
    }

    public void checkTheResultsNumberStep() {
        Logger.info("check that the number of vacancies is greater than the specified value");
        jobSearchPage.getResult().shouldBe(CollectionCondition.sizeGreaterThanOrEqual(1));
    }
}
