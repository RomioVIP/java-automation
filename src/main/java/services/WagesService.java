package services;

import pages.blocks.notAuthorized.MainPage;
import pages.blocks.notAuthorized.WagesSearchPage;
import properties.PropertyManager;
import utils.Logger;

import java.io.FileNotFoundException;

import static com.codeborne.selenide.Selenide.actions;

public class WagesService {

    private final MainPage mainPage = new MainPage();
    private final WagesSearchPage wagesSearchPage = new WagesSearchPage();
    private static final String LOCATION_FOR_SEARCH = PropertyManager.getProperty(PropertyManager.Props.LOCATION_FOR_SEARCH);
    private static final String OCCUPATION_FOR_SEARCH = PropertyManager.getProperty(PropertyManager.Props.OCCUPATION_FOR_SEARCH);

    public void openWagesSearchStep() {
        Logger.info("open the \"Trend analysis\" tab");
        actions().moveToElement(mainPage.getTrendAnalysisBtn()).build().perform();
        Logger.info("click the \"Wages\" button");
        (mainPage.getWagesBtn()).click();
    }

    public void chooseWagesInTheRegionStep() {
        Logger.info("click the options button");
        wagesSearchPage.getOccupationOrLocationBtn().click();
        Logger.info("select \"City or postal code\" in the selector");
        wagesSearchPage.getLocationBtn().click();
        Logger.info("enter the specified city in the input field");
        wagesSearchPage.getInputLocation().clearAndType(LOCATION_FOR_SEARCH, 30000);
        Logger.info("confirm the city");
        wagesSearchPage.getSelectSpecificOccupationOrLocationBtn().click();
        Logger.info("click \"Search\" button");
        wagesSearchPage.getSearchLocationBtn().click();
    }

    public void checkThatWagesInTheRegionIsDisplayedStep() {
        Logger.info("check that the wages results are visible");
        wagesSearchPage.getWagesInTheSpecifiedLocationLabel().isDisplayedWithWait();
    }

    public void chooseOccupationWagesStep() {
        Logger.info("click the options button");
        wagesSearchPage.getOccupationOrLocationBtn().click();
        Logger.info("select \"Occupation\" in the selector");
        wagesSearchPage.getOccupationBtn().click();
        Logger.info("enter the assigned occupation in the input field");
        wagesSearchPage.getInputOccupation().clearAndType(OCCUPATION_FOR_SEARCH, 30000);
        Logger.info("confirm the occupation");
        wagesSearchPage.getSelectSpecificOccupationOrLocationBtn().click();
        Logger.info("click\"Search\" button");
        wagesSearchPage.getSearchOccupationBtn().click();
    }

    public void checkThatOccupationWagesIsVisibleStep() {
        Logger.info("check that the results are visible");
        wagesSearchPage.getWagesOccupationLabel().isDisplayedWithWait();
    }

    public void downloadWadesFileStep() throws FileNotFoundException {
        Logger.info("click \"Download\" button");
        wagesSearchPage.getDownloadWageDataBtn().click();
        Logger.info("download occupation wages result");
        wagesSearchPage.getDownloadResultsBtn().download();
    }
}
