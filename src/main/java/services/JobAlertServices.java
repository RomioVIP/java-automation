package services;

import pages.blocks.authorized.JobAlertPage;
import pages.blocks.notAuthorized.JobSearchPage;
import utils.Logger;

public class JobAlertServices {

    private final JobSearchPage jobSearchPage = new JobSearchPage();
    private final JobAlertPage jobAlertPage = new JobAlertPage();

    public void createJobAlertStep(){
        Logger.info("click the \"Create job alert\"");
        jobSearchPage.getCreateAlertBtn().click();
        Logger.info("check that job alert has been created");
        jobSearchPage.getAlertCreatedLabel().isDisplayedWithWait();
        Logger.info("click the \"Sign on\" button");
        jobSearchPage.getSignOnBtn().click();
        Logger.info("click the \"Alerts\" button");
        jobSearchPage.getAlertsBtn().click();
    }

    public void deleteJobAlertStep(){
        Logger.info("click the \"Delete alert\" button");
        jobAlertPage.getDeleteBtn().click();
        Logger.info("check if no alerts message appears");
        jobAlertPage.getAlertInfoLabel().isDisplayedWithWait();
    }

    public void deactivateJobAlertStep(){
        Logger.info("click \"deactivate alarm\"");
        jobAlertPage.getDeactivateAlertsBtn().click();
        Logger.info("check that the alarm is deactivated");
        jobAlertPage.getAlertInfoLabel().isDisplayedWithWait();
    }

    public void activateJobAlertStep(){
        Logger.info("click the \"inactive\" button");
        jobAlertPage.getInactiveBtn().click();
        Logger.info("click the \"activate alarm\" button");
        jobAlertPage.getReactivateAlertsBtn().click();
        Logger.info("check that the alarm is activated");
        jobAlertPage.getAlertInfoLabel().isDisplayedWithWait();
        Logger.info("click the \"Active\" button");
        jobAlertPage.getActiveBtn().click();
    }
}
