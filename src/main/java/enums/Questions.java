package enums;

public enum Questions {
    Q1("What was the destination of your first trip outside Canada?"),
    Q2("Where did you first meet your significant other?"),
    Q3("Which magazine do you read most often?"),
    Q4("What is your mother's middle name?"),
    Q5("Who was the band/star of the first concert you attended?"),
    Q6("What is your paternal grandfather's first name?"),
    Q7("What is the first name of the best man at your wedding?"),
    Q8("What was the first name of your first love?"),
    Q9("What was the first name of your childhood best friend?"),
    Q10("What is the first name of your oldest cousin?"),
    Q11("What is your favourite fictional character?"),
    Q12("What is your maternal grandmother's first name?"),
    Q13("What is your father's middle name?"),
    Q14("Where did you go for your honeymoon?"),
    Q15("What was the name of your favourite superhero as a child?"),
    Q16("Who was your favourite athlete as a child?"),
    Q17("Who was your most memorable school teacher?"),
    Q18("What is the first name of your mother's oldest sibling?"),
    Q19("In what city or town did your mother and father meet?"),
    Q20("What is the first name of your oldest niece?"),
    Q21("What is the first name of your father's oldest sibling?"),
    Q22("What was the first name of your first manager (boss)?"),
    Q23("What is the first name of the person you went to your prom with?"),
    Q24("Who is your favourite cartoon character?"),
    Q25("In which city or town was your father born?"),
    Q26("What was your favourite subject in school?"),
    Q27("What is the first name of your oldest nephew?"),
    Q28("What is your spouse's/partner's middle name?"),
    Q29("What was the first name of your first roommate?"),
    Q30("Who is your favourite author?"),
    Q31("What is the first musical instrument you ever played?"),
    Q32("What was the make of your first car?"),
    Q33("In which city or town was your mother born?"),
    Q34("What is the middle name of your oldest sibling?"),
    Q35("What is your favourite video game?"),
    Q36("What is your oldest sibling's nickname?"),
    Q37("How old was your father when you were born? (Answer in words, e.g., twenty-six)"),
    Q38("What is your favourite sport?"),
    Q39("What is the name of your first pet?");

    private final String question;

    Questions(String question) {
        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    public static Questions findByTakenText(String takenText){
        for(Questions q : values()){
            if( q.getQuestion().equals(takenText)){
                return q;
            }
        }
        return null;
    }
}
