package properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyManager {

    public enum Props {
        EMAIL,
        PASSWORD,
        ANSWER_Q1,
        ANSWER_Q4,
        ANSWER_Q11,
        ANSWER_Q24,
        ANSWER_Q39,
        EDUCATION_PROGRAM_NAME,
        FRENCH_LOGO,
        PROFESSION_FOR_SEARCH,
        MAIL_URL,
        USER_FIRST_NAME,
        USER_LAST_NAME,
        LOCATION_FOR_SEARCH,
        OCCUPATION_FOR_SEARCH
    }

    private static final Properties properties = System.getProperties();

    static {
        try {
            properties.load(new FileInputStream(new File(System.getProperty("user.dir")
                    + "/src/main/resources/constants.properties")));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static String getProperty(Props prop){
        return properties.getProperty(prop.name());
    }

}
